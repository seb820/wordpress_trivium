<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
    <meta name="description" content="wish i pass the audit so that i dont see this red line here.">
    <meta name="theme-color" content="#317EFB"/>
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
</head>
<body>
    <header>
    <h2> index </h2>
    </header>
    <div class="big-container">
    <h3>A</h3>
<ul>
    <?php
// Sort the posts array by title in alphabetical order
usort($posts, function($a, $b) {
//strcmp compares two strings
    return strcmp($a->post_title, $b->post_title);
});
?>
<ul>
    <?php foreach($posts as $post) :
        setup_postdata($post);
    ?>
        <li>
            <a href="<?php the_permalink(); ?>">
                <p><?php the_title(); ?></p>
                <?php the_field('animals'); ?>
            </a>
        </li>
    <?php endforeach; ?>
</ul>

<?php wp_reset_postdata(); ?>
</div>
</body>
</html>